#!/bin/zsh

######################
## Default programs ##
######################

export MANPAGER="nvim +Man!"
export VISUAL="nvim"
export EDITOR="nvim"
export TERMINAL="alacritty"
export BROWSER="qutebrowser"
export LAUNCHER="dmenu"
export VIDEO="mpv"
export MUSIC="mpc"
export MUSICGUI="ncmpcpp"
export SESSIONMGR="tmux"
export MULTIPLXR="tmux"
export LOCKER="slock"
export FILEMGR=""
export MAIL="neomutt"
export WALLPAPERS="$HOME/Pics/Wallpapers"

export WLR_NO_HARDWARE_CURSORS=1
export QT_QPA_PLATFORM=wayland
export GTK_THEME="Adwaita:dark"

####################
## Path additions ##
####################

export PATH="$CARGO_HOME/bin/:$HOME/.local/bin:$XDG_CONFIG_HOME/platformio/penv/bin:$PATH"
export MANPATH="/usr/local/man:$MANPATH"

#################
## ~/ Clean-up ##
#################

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

export DOTFILES="$HOME/.dotfiles"

#export VIFM_SERVER_NAME=vifm
export LESSHISTFILE=-
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/pythonrc.py"

export IDF_TOOLS_PATH="$HOME/.local/bin"

export PASSWORD_STORE_DIR="$XDG_DATA_HOME/password-store"

export ANDROID_SDK_HOME="$XDG_CONFIG_HOME/android"
export ANDROID_EMULATOR_HOME="$XDG_DATA_HOME"/android/
export ANDROID_AVD_HOME="$XDG_DATA_HOME"/android/
export ADB_VENDOR_KEY="$XDG_CONFIG_HOME"/android

export HISTFILE="$XDG_DATA_HOME/history"

export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"

export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java

export TEXMFHOME=$XDG_DATA_HOME/texmfexport TEXMFVAR=$XDG_CACHE_HOME/texlive/texmf-var
export TEXMFCONFIG=$XDG_CONFIG_HOME/texlive/texmf-config

export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export XSERVERRC="$XDG_CONFIG_HOME"/X11/xserverrc

export TS3_CONFIG_DIR="$XDG_CONFIG_HOME/ts3client"

export MOZ_USE_XINPUT2="1"		# Mozilla smooth scrolling/touchpads.
export _JAVA_AWT_WM_NONREPARENTING=1	# Fix for Java applications in dwm
export AWT_TOOLKIT=MToolkit

export BAT_THEME="gruvbox-dark"
export CARGO_HOME="$XDG_DATA_HOME"/cargo

# export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GOPATH="$XDG_DATA_HOME"/go
export WEECHAT_HOME="$XDG_CONFIG_HOME/weechat"
export CONAN_USER_HOME="$XDG_CONFIG_HOME/conan"
export PLATFORMIO_CORE_DIR="$XDG_CONFIG_HOME/platformio"
export TREE_SITTER_DIR="${XDG_CONFIG_HOME}/tree-sitter"
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export NOTMUCH_CONFIG="$XDG_CONFIG_HOME"/notmuch/notmuchrc
export NMBGIT="$XDG_DATA_HOME"/notmuch/nmbug
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export GRIM_DEFAULT_DIR=$HOME/Pictures/Screenshots

export ZELLIJ_CONFIG_DIR="$XDG_CONFIG_HOME/zellij"

#export XDG_CURRENT_DESKTOP=Unity
#export QT_QPA_PLATFORMTHEME=gtk2
# Scaling
# export QT_AUTO_SCREEN_SCALE_FACTOR=1
# export GDK_SCALE=2
# export GDK_DPK_SCALE=0.5

#export BW_SESSION=2nUI7S3Cq0p2lrTdYeNexjQMj9t3l4
