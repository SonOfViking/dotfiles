# dotfiles

My dotfiles

## Getting started

- Clone this repo
- Set the DOTFILES envvar to the path of the just cloned repo
```set DOTFILES path/to/dotfiles```
- Install stow
```
sudo pacman -S stow # arch-based
sudo apt install stow # debian-based
```
- if not done so already make the install and clean scripts executable
```chmod +x install clean```
- run the install script
```./install```
    NOTE: that it 'installs' the dotfiles in your .config/.local folder, it does not install any other software.
- Enjoy!
