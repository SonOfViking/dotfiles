{
    description = "SonOfViking's flake";
    inputs = {
        nixpkgs.url = "nixpkgs/nixos-unstable";
        home-manager = {
            url = "github:nix-community/home-manager/master";
            inputs.nixpkgs.follows = "nixpkgs";
        };
        wezterm.url = "github:wez/wezterm?dir=nix";
    };
    outputs = inputs @ { self, nixpkgs, home-manager, ... }:
        let
          lib = nixpkgs.lib;
          system = "x86_64-linux";
          pkgs = nixpkgs.legacyPackages.${system};
        in {
            nixosConfigurations = {
                ASGARD = lib.nixosSystem {
                    inherit system;
                    modules = [ ./configuration.nix ];
                };
            };
            homeConfigurations = {
                sonofviking = home-manager.lib.homeManagerConfiguration {
                    inherit pkgs;
                    extraSpecialArgs = { inherit inputs; };
                    modules = [ ./home.nix ];
                };
            };
        };
}
