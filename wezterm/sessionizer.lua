local wezterm = require("wezterm")
local act = wezterm.action

local M = {}

---@param program string
---@return string | nil
local function get_program(program)
    local handle = io.popen("which " .. program)
    if handle == nil then
        return nil
    end

    local result = handle:read("a")
    handle:close()

    return result:gsub("[\r\n]", "")
end

local fd = get_program("fd")

local rootPath = wezterm.home_dir .. "/Projects"


M.toggle = function(window, pane)
  local projects = {}

  wezterm.log_info("using" .. fd .. " and " .. rootPath);
  local success, stdout, stderr = wezterm.run_child_process({
    fd,
    "-HI",
    "-td",
    "^.git$",
    "--max-depth=4",
    rootPath,
    wezterm.home_dir .. "/.dotfiles"
  })

  if not success then
    wezterm.log_error("Failed to run fd: " .. stderr)
    return
  end

  for line in stdout:gmatch("([^\n]*)\n?") do
    local project = line:gsub("/.git/$", "")
    local label = project
    local id = project:gsub(".*/", "")
    table.insert(projects, { label = tostring(label), id = tostring(id) })
  end

  window:perform_action(
    act.InputSelector({
      action = wezterm.action_callback(function(win, _, id, label)
        if not id and not label then
          wezterm.log_info("Cancelled")
        else
          wezterm.log_info("Selected " .. label)
          win:perform_action(
            act.SwitchToWorkspace({ name = id, spawn = { cwd = label } }),
            pane
          )
        end
      end),
      fuzzy = true,
      title = "Select project",
      choices = projects,
    }),
    pane
  )
end

return M
