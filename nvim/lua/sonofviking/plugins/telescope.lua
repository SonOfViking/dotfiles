return {
  {
    "nvim-telescope/telescope.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' }
    },
    config = function()
      require("telescope").setup({
        defaults = {
          file_ignore_patters = "^.git/"
        },
        pickers = {
          find_files = {
            hidden = true,
            theme = "ivy"
          },
          current_buffer_fuzzy_find = {
            theme = "ivy"
          }
        },
        extensions = {
          fzf = {}
          -- TODO
          --require('telescope').load_extension('dap')
          --require('telescope').load_extension('undo')
          --require("telescope").load_extension('harpoon')


        }
      })
      require('telescope').load_extension('fzf')
      vim.keymap.set('n', '<leader>?', require('telescope.builtin').oldfiles, { desc = '[?] Find recently opened files' })
      vim.keymap.set('n', '<leader><space>', require('telescope.builtin').buffers, { desc = '[ ] Find existing buffers' })
      vim.keymap.set('n', '<leader>/', function() require('telescope.builtin').current_buffer_fuzzy_find() end,
        { desc = '[/] Fuzzily search in current buffer]' })
      vim.keymap.set('n', '<C-p>', function() require('telescope.builtin').find_files({ hidden = true }) end,
        { desc = '[S]earch [F]iles' })
      vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags, { desc = '[S]earch [H]elp' })
      vim.keymap.set('n', '<leader>sw', require('telescope.builtin').grep_string, { desc = '[S]earch current [W]ord' })
      vim.keymap.set('n', '<leader>sg', require('telescope.builtin').live_grep, { desc = '[S]earch by [G]rep' })
      vim.keymap.set('n', '<leader>sd', require('telescope.builtin').diagnostics, { desc = '[S]earch [D]iagnostics' })
      vim.keymap.set('n', '<leader>ut', '<cmd>Telescope undo<CR>', { desc = '[S]earch [D]iagnostics' })
      vim.keymap.set("n", "<space>en",
        function() require('telescope.builtin').find_files { cwd = vim.fn.stdpath("config") } end)
    end
  },

}
