return {
    "nvim-lua/plenary.nvim",
    "nvim-tree/nvim-web-devicons",
    {
        "folke/zen-mode.nvim",
        opts = {
            window = {
                backdrop = 0.5, -- shade the backdrop of the Zen window. Set to 1 to keep the same as Normal
                width = 0.5,
            },
            plugins = {
                options = {
                    enabled = true,
                    ruler = true,
                    showcmd = false,
                    laststatus = 0, -- turn off the statusline in zen mode
                    -- laststatus = 3, -- keep statusline in zen mode
                },
                twilight = { enabled = false },
                todo = { enabled = false },
                wezterm = {
                    enabled = false,
                    font = "+4",
                },
                neovide = {
                    enabled = false,
                    scale = 1.2,
                    disable_animations = {
                        neovide_animation_length = 0,
                        neovide_cursor_animate_command_line = false,
                        neovide_scroll_animation_length = 0,
                        neovide_position_animation_length = 0,
                        neovide_cursor_animation_length = 0,
                        neovide_cursor_vfx_mode = "",
                    }
                },
            },
            -- callback where you can add custom code when the Zen window opens
            on_open = function(win)
            end,
            -- callback where you can add custom code when the Zen window closes
            on_close = function()
            end,
        }
    },
    "folke/trouble.nvim",
    {
        "numToStr/Comment.nvim",
        opts = {},
        lazy = false,
    },
    "debugloop/telescope-undo.nvim",
    {
        "TimUntersberger/neogit",
        dependencies = {
            "nvim-lua/plenary.nvim"
        },
        config = function()
            require('neogit').setup()
        end
    },
    {
        'echasnovski/mini.align',
        version = '*',
        config = function()
            require('mini.align').setup()
        end
    },
    {
        "sindrets/diffview.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim"
        }
    },
}
