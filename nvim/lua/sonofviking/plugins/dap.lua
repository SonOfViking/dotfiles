return {
    {
        'mfussenegger/nvim-dap',
        -- Could be in the keys clause
        config = function()
            vim.keymap.set('n', '<leader>dtb', '<cmd>lua require"dap".toggle_breakpoint()<CR>')
            vim.keymap.set('n', '<leader>dct', '<cmd>lua require"dap".continue()<CR>')
            vim.keymap.set('n', '<leader>dso', '<cmd>lua require"dap".step_over()<CR>')
            vim.keymap.set('n', '<leader>dsi', '<cmd>lua require"dap".step_into()<CR>')
            vim.keymap.set('n', '<leader>dcd', '<cmd>lua require"dap".disconnect()<CR>')
            local dap = require('dap')
            if not dap.adapters then dap.adapters = {} end
            -- dap.adapters["probe-rs-debug"] = {
            --     type = "server",
            --     -- port = "${port}",
            --     port = "6969",
            --     executable = {
            --         command = vim.fn.expand "$CARGO_HOME/bin/probe-rs",
            --         args = { "dap-server", "--port", "6969" },
            --     },
            -- }
            local dap_cortex_debug = require('dap-cortex-debug')
            dap.configurations.rust = {
                dap_cortex_debug.openocd_config {
                    name = 'Example debugging with OpenOCD',
                    cwd = '${workspaceFolder}',
                    executable = '${workspaceFolder}/build/app',
                    configFiles = { '${workspaceFolder}/build/openocd/connect.cfg' },
                    gdbTarget = 'localhost:3333',
                    rttConfig = dap_cortex_debug.rtt_config(0),
                    showDevDebugOutput = false,
                },
            }

            -- Connect the probe-rs-debug with rust files. Configuration of the debugger is done via project_folder/.vscode/launch.json
            require("dap.ext.vscode").type_to_filetypes["probe-rs-debug"] = { "rust" }
            require("dap.ext.vscode").load_launchjs(nil, { rt_lldb = { "rust" }, ["probe-rs-debug"] = { "rust" } })
            -- Set up of handlers for RTT and probe-rs messages.
            -- In addition to nvim-dap-ui I write messages to a probe-rs.log in project folder
            -- If RTT is enabled, probe-rs sends an event after init of a channel. This has to be confirmed or otherwise probe-rs wont sent the rtt data.
            dap.listeners.before["event_probe-rs-rtt-channel-config"]["plugins.nvim-dap-probe-rs"] = function(session,
                                                                                                              body)
                local utils = require "dap.utils"
                utils.notify(
                    string.format('probe-rs: Opening RTT channel %d with name "%s"!', body.channelNumber,
                        body.channelName)
                )
                local file = io.open("probe-rs.log", "a")
                if file then
                    print("Should be printing to prober-rs.log: "..body)
                    file:write(
                        string.format(
                            '%s: Opening RTT channel %d with name "%s"!\n',
                            os.date "%Y-%m-%d-T%H:%M:%S",
                            body.channelNumber,
                            body.channelName
                        )
                    )
                end
                if file then file:close() end
                session:request("rttWindowOpened", { body.channelNumber, true })
            end
            -- After confirming RTT window is open, we will get rtt-data-events.
            -- I print them to the dap-repl, which is one way and not separated.
            -- If you have better ideas, let me know.
            dap.listeners.before["event_probe-rs-rtt-data"]["plugins.nvim-dap-probe-rs"] = function(_, body)
                local message =
                    string.format("%s: RTT-Channel %d - Message: %s", os.date "%Y-%m-%d-T%H:%M:%S", body.channelNumber,
                        body.data)
                local repl = require "dap.repl"
                repl.append(message)
                local file = io.open("probe-rs.log", "a")
                if file then file:write(message) end
                if file then file:close() end
            end
            -- Probe-rs can send messages, which are handled with this listener.
            dap.listeners.before["event_probe-rs-show-message"]["plugins.nvim-dap-probe-rs"] = function(_, body)
                local message = string.format("%s: probe-rs message: %s", os.date "%Y-%m-%d-T%H:%M:%S", body.message)
                local repl = require "dap.repl"
                repl.append(message)
                local file = io.open("probe-rs.log", "a")
                if file then file:write(message) end
                if file then file:close() end
            end
            -- dap.adapters.codelldb = {
            --     type = 'server',
            --     port = "13000",
            --     executable = {
            --         -- CHANGE THIS to your path!
            --         command = '/home/sonofviking/.local/share/nvim/mason/bin/codelldb',
            --         args = { "--port", "13000" },
            --
            --         -- detached = false,
            --     }
            -- }
            -- dap.configurations.cpp = {
            --     {
            --         name = "Launch file",
            --         type = "codelldb",
            --         request = "launch",
            --         program = function()
            --             -- TODO make it so that this is automagically picked up
            --             return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
            --         end,
            --         cwd = '${workspaceFolder}',
            --         stopOnEntry = false,
            --     },
            -- }
            -- dap.configurations.c = dap.configurations.cpp
            -- dap.configurations.rust = dap.configurations.cpp
        end,
    },
    { 'abayomi185/nvim-dap-probe-rs',
    config = function ()
        require('dap-probe-rs').setup()
    end
},
    {
        'theHamsta/nvim-dap-virtual-text',
        config = function()
            require("nvim-dap-virtual-text").setup()
        end
    },
    {
        "rcarriga/nvim-dap-ui",
        dependencies = {
            "mfussenegger/nvim-dap",
            "nvim-neotest/nvim-nio"
        },
        config = function()
            vim.keymap.set('n', '<leader>duc', '<cmd>lua require"dapui".close()<CR>')

            local dap, dapui = require("dap"), require("dapui")
            dapui.setup(
                      {
                        controls = {
                          element = "repl",
                          enabled = true,
                          icons = {
                            disconnect = "",
                            pause = "",
                            play = "",
                            run_last = "",
                            step_back = "",
                            step_into = "",
                            step_out = "",
                            step_over = "",
                            terminate = ""
                          }
                        },
                        element_mappings = {},
                        expand_lines = true,
                        floating = {
                          border = "single",
                          mappings = {
                            close = { "q", "<Esc>" }
                          }
                        },
                        force_buffers = true,
                        icons = {
                          collapsed = "",
                          current_frame = "",
                          expanded = ""
                        },
                        layouts = { {
                            elements = { {
                                id = "scopes",
                                size = 0.25
                              }, {
                                id = "breakpoints",
                                size = 0.25
                              }, {
                                id = "stacks",
                                size = 0.25
                              }, {
                                id = "watches",
                                size = 0.25
                              } },
                            position = "left",
                            size = 40
                          }, {
                            elements = {
                                {
                                id = "repl",
                                size = 0.5
                              },
                              {
                                  id = "rtt",
                                  size = 0.5
                              },
                              -- {
                              --   id = "console",
                              --   size = 0.5
                              -- }
                          },
                            position = "bottom",
                            size = 10
                          } },
                        mappings = {
                          edit = "e",
                          expand = { "<CR>", "<2-LeftMouse>" },
                          open = "o",
                          remove = "d",
                          repl = "r",
                          toggle = "t"
                        },
                        render = {
                          indent = 1,
                          max_value_lines = 100
                        }
                      })
            dap.listeners.before.attach.dapui_config = function()
                dapui.open()
            end
            dap.listeners.before.launch.dapui_config = function()
                dapui.open()
            end
            dap.listeners.before.event_terminated.dapui_config = function()
                dapui.close()
            end
            dap.listeners.before.event_exited.dapui_config = function()
                dapui.close()
            end
        end
    },
    "nvim-telescope/telescope-dap.nvim",
    {
        "jedrzejboczar/nvim-dap-cortex-debug",
        dependencies = {
            "mfussenegger/nvim-dap",
        },
        config = function()
            require("dap-cortex-debug").setup {
                debug = false,  -- log debug messages
                -- path to cortex-debug extension, supports vim.fn.glob
                -- by default tries to guess: mason.nvim or VSCode extensions
                extension_path = nil,
                lib_extension = nil, -- shared libraries extension, tries auto-detecting, e.g. "so" on unix
                node_path = "node", -- path to node.js executable
                dapui_rtt = true, -- register nvim-dap-ui RTT element
                -- make :DapLoadLaunchJSON register cortex-debug for C/C++, set false to disable
                dap_vscode_filetypes = { "c", "cpp", "rust" },
                rtt = {
                    buftype = "Terminal", -- "Terminal" or "BufTerminal" for terminal buffer vs normal buffer
                },
            }
        end
    },
}
