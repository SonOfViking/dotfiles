return {
    "ThePrimeagen/harpoon",
    branch = "harpoon2",
    dependencies = { "nvim-lua/plenary.nvim" },
    config = function()
        local harpoon = require("harpoon")

        -- REQUIRED
        harpoon:setup()
        -- REQUIRED

        vim.keymap.set("n", "<leader>a", function() harpoon:list():add() end)
        vim.keymap.set("n", "<C-e>", function() harpoon.ui:toggle_quick_menu(harpoon:list()) end)

        vim.keymap.set("n", "<C-h>", function() harpoon:list():select(1) end)
        vim.keymap.set("n", "<C-t>", function() harpoon:list():select(2) end)
        vim.keymap.set("n", "<C-n>", function() harpoon:list():select(3) end)
        vim.keymap.set("n", "<C-s>", function() harpoon:list():select(4) end)

        -- Toggle previous & next buffers stored within Harpoon list
        vim.keymap.set("n", "<C-S-P>", function() harpoon:list():prev() end)
        vim.keymap.set("n", "<C-S-N>", function() harpoon:list():next() end)
        return {}
    end
}
--local ok, harpoon = pcall(require, "harpoon")
--
--if not ok then return end
--
--local ui = require("harpoon.ui")
--local term = require("harpoon.term")
--local mark = require("harpoon.mark")
--local cmd_ui = require("harpoon.cmd-ui");
--
--vim.keymap.set("n", "<leader>e", cmd_ui.toggle_quick_menu)
--vim.keymap.set("n", "<leader>th", function() term.sendCommand(1, 1) end )
--vim.keymap.set("n", "<leader>tt", function() term.sendCommand(1, 2) end )
--
--vim.keymap.set("n", "<C-e>", ui.toggle_quick_menu)
--vim.keymap.set("n", "<leader>a", mark.add_file)
--vim.keymap.set("n", "<C-h>", function() ui.nav_file(1) end )
--vim.keymap.set("n", "<C-t>", function() ui.nav_file(2) end )
--vim.keymap.set("n", "<C-n>", function() ui.nav_file(3) end )
--vim.keymap.set("n", "<C-s>", function() ui.nav_file(4) end )
--
--vim.keymap.set("n", "<leader>tu", function() term.gotoTerminal(1) end )
--vim.keymap.set("n", "<leader>te", function() term.gotoTerminal(2) end )
--local global_settings = {
--    -- sets the marks upon calling `toggle` on the ui, instead of require `:w`.
--    save_on_toggle = false,
--
--    -- saves the harpoon file upon every change. disabling is unrecommended.
--    save_on_change = true,
--
--    -- sets harpoon to run the command immediately as it's passed to the terminal when calling `sendCommand`.
--    enter_on_sendcmd = true,
--
--    -- closes any tmux windows harpoon that harpoon creates when you close Neovim.
--    tmux_autoclose_windows = false,
--
--    -- filetypes that you want to prevent from adding to the harpoon list menu.
--    excluded_filetypes = { "harpoon" },
--
--    -- set marks specific to each git branch inside git repository
--    mark_branch = false,
--
--    -- enable tabline with harpoon marks
--    tabline = false,
--    tabline_prefix = "   ",
--    tabline_suffix = "   ",
--}
--harpoon.setup(global_settings);
