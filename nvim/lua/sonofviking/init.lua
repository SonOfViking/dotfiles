--require("sonofviking.plugins")
require("sonofviking.remaps")
require("sonofviking.options")
require("sonofviking.viking_runner");

local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
    group = highlight_group,
    pattern = '*',
    callback = function()
        vim.highlight.on_yank({
            higroup = 'IncSearch',
            timeout = 40,
        })
    end,
})

local SonOfVikingGroup = vim.api.nvim_create_augroup('SonOfViking', { clear = true })
vim.api.nvim_create_autocmd({"BufWritePre"}, {
    group = SonOfVikingGroup,
    pattern = "*",
    command = [[%s/\s\+$//e]],
})
