--local popup = require("plenary.popup")

-- Do we need some configs?

local M = {}
M.channel_id = nil
M.term_buf_id = nil

M.read_commands = function()
    local fname = "commands.list";

    local commands = {};
    local file = io.open(fname, "r");
    if not file then
        return nil;
    end

    for value in file:lines("*l") do
        table.insert(commands, value);
    end

    return commands;
end

M.open_term = function()
    print(vim.api.nvim_get_current_buf())
    if not M.term_buf_id then
        vim.cmd.term()
        M.term_buf_id = vim.api.nvim_get_current_buf()
    else
        local current_win = vim.api.nvim_get_current_win();
        vim.api.nvim_win_set_buf(current_win, M.term_buf_id);
    end
    M.channel_id = vim.bo.channel
end

M.write_user_set_commands = function(commands)
    -- Do something with $XDG_DATA_HOME/nvim/....
    local fname = "commands.list";

    local file = io.open(fname, "w");
    if file == nil then return end

    for _, value in ipairs(commands) do
        file:write(value .. "\n");
    end
    file:close();
end

M.execute_command = function(command)
    if not command then
        print("No command selected");
        return;
    end
    M.open_term()
    --vim.uv.sleep(2)
    vim.fn.chansend(M.channel_id, { command })
end

-- Set commands using UI
-- Set whether commands should be buffered or not
-- Send to quickfix list

local function viking_runner()
    -- TODO: use harpoon for ui stuff?
    local commands = M.read_commands();

    if not commands then
        local command =  vim.fn.input({ prompt = "Enter command: " })
        if not string.find(command, "\r\n") then
            command = command .. "\r\n"
        end
        commands = { command }
        M.write_user_set_commands(commands)
    end

    if #commands == 1 then
        M.execute_command(commands[1])
        return
    end

    -- local buf = api.nvim_create_buf(false, false);
    -- popup.create(buf, {});

    vim.ui.select(commands, { prompt = "Select command to execute:" }, M.execute_command);
end

vim.api.nvim_create_user_command("VikingRunner", function(_) viking_runner(); end, {});

vim.api.nvim_create_autocmd('TermOpen', {
    group = vim.api.nvim_create_augroup("custom-term-open", { clear = true }),
    callback = function()
        vim.opt.number = false
        vim.opt.relativenumber = false
    end
})

vim.keymap.set("n", "<leader>st", function()
    M.open_term()
end)

vim.keymap.set("n", "<leader>b", function()
    viking_runner()
end)
