local SonOfVikingGroup = vim.api.nvim_create_augroup('ExportOnSave', { clear = true })
vim.api.nvim_create_autocmd({"BufWritePre"}, {
    group = SonOfVikingGroup,
    pattern = "*",
    command = [[silent make]],
})
