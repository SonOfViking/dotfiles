{ config, inputs, pkgs, ... }:

{
  home.username = "sonofviking";
  home.homeDirectory = "/home/sonofviking";

  home.stateVersion = "24.05"; # WARN: Do not change, may break home-manager

  home.packages = with pkgs; [
      eza
      neovim
      fd
      ripgrep
      w3m
      rsync
      unzip
      btop
      ripgrep
      nerdfonts
      git delta
      starship
      zellij
      zsh zsh-fzf-tab zsh-autosuggestions
      # luakit/qutebrowser
      #direnv nix-direnv

      # hyprland TODO: intall via flake?
      # pipewire TODO: should that be managed here?
  ];
  #programs.neovim = {
      #defaultEditor = true;
  #     enable = true;
  # };
  # programs.wezterm = {
  #   enable = true;
  #   package = inputs.wezterm.packages.${pkgs.system}.default;
  # };
  programs.zsh = {
    enable = true;
    dotDir = ".config/zsh";
    enableCompletion = true;
    syntaxHighlighting.enable = true;
    #autosuggestions = true;
  };

  home.file = {
    ".config/aliases" = {
        source = ./aliases;
    };
    # ".config/nvim" = {
    #     source = ./nvim;
    #     recursive = true;
    # };
    ".config/hypr" = {
        source = ./hypr;
    };
    ".config/zsh" = {
        source = ./zsh;
    };
    ".config/zellij" = {
        source = ./zellij;
    };
    ".config/paru" = {
        source = ./paru;
    };
    ".config/git" = {
        source = ./git;
    };
    ".config/starship.toml" = {
        source = ./starship.toml;
    };
    ".config/zathura" = {
        source = ./zathura;
    };
    # This may be set once I have decided on a more permanent configuration
    # ".config/wezterm" = {
    #     source = ./wezterm;
    #     recursive = true;
    # };
    ".config/nix" = {
        source = ./nix;
    };
  };
  #TODO: add wofi
  #TODO: add waybar
  # TODO: user dirs?

  # Home Manager can also manage your environment variables through
  # 'home.sessionVariables'. These will be explicitly sourced when using a
  # shell provided by Home Manager. If you don't want to manage your shell
  # through Home Manager then you have to manually source 'hm-session-vars.sh'
  # located at either
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  ~/.local/state/nix/profiles/profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/sonofviking/etc/profile.d/hm-session-vars.sh
  #
  home.sessionVariables = {
    ZDOTDIR = ".config/zsh";
    # EDITOR = "emacs";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
